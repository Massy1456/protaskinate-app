class Task {
  bool isComplete;
  String taskName;

  Task(this.isComplete, this.taskName);

  void toggleDone() {
    isComplete = !isComplete;
  }
}
